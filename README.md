This ansible playbook covers common workflow used during deployment of new
workloads under multiple Cisco ACI fabrics using Multi-site Orchestrator.

The genral workflow will be:
- Configure Interface Policy groups on Cisco ACI Frbric.
- Configure Interface Select profiles on Cisco ACI Fabric.
- Configure Static Binding on Cisco MSO under Schema & template site local.
- Deploy MSO Template.

Master playbook contains multiple imported playbooks specific to Cisco ACI.
This gives flexibility to executing specific playbook only if required.

Tested ansible Version ==> 6.3.0 & Python Version ==> 3.8.0

To be able to work with Cisco MSO installed on Nexus dashboard, install below
latest collections from ansible-galaxy:

ansible-galaxy collection install cisco.mso -p ./collections --force
ansible-galaxy collection install cisco.nd -p ./collections --force

Additional Libraries Required:

csv_to_facts
xls_to_csv

Description:

Altogether, we have three playbooks, two for Cisco ACI specific while one for
Cisco MSO. Two Cisco ACI playbooks are used for deploying ACI interface policy
groups & interface select polices while master playbook is used for deploying 
EPG static binding on MSO. Two ACI workbooks are imported into MSO playbook
for end-to-end configuration as well as we can also use idivisual playbook
seperatly also.

Each playbook will read "aci_mso_deploy_wb.xlsx" file. This will contains
multiple sheets to describe all required configuration. This multiple
sheets will be converted to CSV file by indivisual playbook & converted 
to asnible facts. These facts will be used by respective playbook
modules to configure ACI as well as MSO.

